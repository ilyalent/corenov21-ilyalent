package com.epam.learn.l3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Main {
    private static boolean isTrue=true;
    // if:
    public static void main(String[] args) {
//        String line = null;
//        //reader - позволяет считывать с консоли
//        try (BufferedReader reader=new BufferedReader(new InputStreamReader(System.in))) {
//            // readLine - считать линию с консоли
//            line=reader.readLine();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        int finalResult=Integer.parseInt(Objects.requireNonNull(line));

        //в такой конструкции если приходит результат отличный от 3 - у нас все равно красный
        // 1 - зеленый, 2 - желтый, 3 - красный
//        if (finalResult==1) {
//            System.out.println("green");
//        } else if (finalResult==2) {
//            System.out.println("yellow");
//        } else {
//            System.out.println("red");
//        }

        // Object сравнивается через equals!, enum можно сравнивать через ==, потому что они синглтоны.
        Colours colours=Colours.RED;
        if (getColorByTime(colours) == Colours.GREEN ) {
            System.out.println("GREEN");
        } else if (getColorByTime(colours) == Colours.YELLOW) {
            System.out.println("YELLOW");
        } else if (getColorByTime(colours) == Colours.RED){
            System.out.println("RED");
        } else {
            throw new RuntimeException();
        }
        System.out.println(Colours.GREEN);
    }
    private static Colours getColorByTime (Colours colour) {
        return colour;
    }
}
