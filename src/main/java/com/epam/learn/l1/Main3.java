package com.epam.learn.l1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main3 {
    Byte aByte;
    Short aShort;
    Integer aInteger; //null
    Long aLong;

    Float aFloat;
    Double aDouble;

    Boolean aBoolean;

    Character character;

    public static void main(String[] args) {
        /*int a=5;

        List list=new ArrayList();
        list.add(a);*/

        //ctrl+alt+L - выравнивание кода
        /*        System.out.println(String.valueOf(a));*/

        System.out.println(Integer.valueOf("5") + 2);

        System.out.println(Integer.reverse(521));

        System.out.println(new StringBuilder("hi").reverse().toString());
        System.out.println(new Main3().character);
    }

    private int getSum(int a) {
        return 1;
    }

    private int getSumAB(int a, int b) {
        return a + b;
    }
}
