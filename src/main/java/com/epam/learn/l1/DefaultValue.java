package com.epam.learn.l1;

public class DefaultValue {

    byte defaultByte; //(byte) 0
    short defaultShort; //0
    int dInt; //0
    long defaultLong; //0

    float defaultFloat; //0.0
    double defaultDouble; //0.0

    boolean fBoolean; //false

    char dChar; //'\u0000' (null)

    public static void main(String[] args) {
        int dInt;
        System.out.println(new DefaultValue().dChar);
    }
}
