package com.epam.learn.l1;

public class PrimitiveExample {
    //целые
    //ctrl+shift+стрелочка вверх вниз
    byte someByte; //-128 127 8 бит
    short someShort; //-2^15 2^15-1 16 бит
    int number=1; //-2^31 2^31-1 32 бита
    long bigNumber; //-2^63 2^63-1 64 бита

    //дробные;
    //https://ru.wikipedia.org/wiki/IEEE_754-2008
    float nFloat; //32 бита
    double nDouble; //64 бита

    //символьные
    char someChar; //16 бит

    //логический
    //https://ru.stackoverflow.com/questions/625400/%D0%A0%D0%B0%D0%B7%D0%BC%D0%B5%D1%80-%D1%82%D0%B8%D0%BF%D0%B0-boolean
    boolean isTrue; //размер зависит от реализации конкретной JVM
}
