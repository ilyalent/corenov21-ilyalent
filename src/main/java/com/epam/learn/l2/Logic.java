package com.epam.learn.l2;

public class Logic {
    // default = false
    private boolean paramA=true;
    private boolean paramB=false;
    private boolean paramC=true;

    public static void main(String[] args) {
        Main5 main5 =new Main5();

        Logic logic=new Logic();
        System.out.println(logic.checkResultFirst(logic.paramA, logic.paramB, logic.paramC));
        System.out.println(logic.checkResultSecond(logic.paramA, logic.paramB));
        System.out.println(logic.checkResultThird(logic.paramC, logic.paramB));
        System.out.println(logic.checkResultForth(logic.paramA, logic.paramB, logic.paramC));

    }

    // &&, ||, >, <, ==, !=, <=, >=

    // 1) A && B || C
    private boolean checkResultFirst(boolean a, boolean b, boolean c) {
        return a && b || c;
    }

    // 2) A && B
    // 3) C || B
    // 4) A || B && C
    private boolean checkResultSecond(boolean a, boolean b) {
        return a && b;
    }

    private boolean checkResultThird(boolean c, boolean b) {
        return c || b;
    }

    private boolean checkResultForth(boolean a, boolean b, boolean c) {
        return a || b && c;
    }




}
