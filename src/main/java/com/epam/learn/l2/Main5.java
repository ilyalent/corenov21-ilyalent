package com.epam.learn.l2;

class Main5 {

    int count=5;
    volatile int count2;
    transient int count3; //игнор при сериализации

    //класс может быть public, default, final, abstract
    //public - доступен из любого класса проекта
    //default - доступен внутри пакета
    //abstract - мы не можем создать или получить обьект данного класса

}
