package com.epam.learn.l2;

public class Initial {
    public Initial(int count) { //3
        this.count = count;
    }

    private int count = 0; // 1

    {
        count=10; //2
    }

    public static void main(String[] args) {
        Initial in=new Initial(5);
        System.out.println(in.count);
    }
}
