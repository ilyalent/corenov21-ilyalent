package com.epam.learn.l2;

public class Main2 {

    public Main2(int count) {

    }

    public Main2() {

    }

    public static void main(String[] args) {
        Main2 main2=new Main2(); // main2-ссылочная переменная(в стеке),  new - выделение памяти в heap(куче), Main2 - вызов конструктор класса
        System.out.println(main2.isEven(2));
        System.out.println(main2.isOdd(5));
    }

    private boolean isEven(int param) {
        return param%2==0;
    }

    private boolean isOdd(int param) {
        return param%2!=0;
    }
}
