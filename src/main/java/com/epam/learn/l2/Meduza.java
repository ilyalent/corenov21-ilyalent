package com.epam.learn.l2;

public class Meduza extends Animal { //extends - потомок получает все свойства родителя
    @Override //
    public void eat() {
        System.out.println("He's body is his mouth");
    }

    public static void main(String[] args) {
        Animal animal=new Meduza();
        animal.eat();
    }
}
