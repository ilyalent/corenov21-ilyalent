package com.epam.learn.l2;

public class Example {

    final int count;//final переменные обязаны быть проинициализированны

    {
        count=10;
    }

/*    public Example(int count) {
        this.count=count;
    }*/

    public static void main(String[] args) {

    }

    void checkSomething() { // private, default, protected(данный метод будет виден в пакете и в потомках), abstract, synchronized, strictfp,
        //final(нельзя переопределять данный метод), native(для нативных языков), static

    }
}
