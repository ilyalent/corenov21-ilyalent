package com.epam.learn.test;

public class TaxiCar extends Car {
    private String model;

    public TaxiCar(String model) {
        this.model = model;
    }

    @Override
    public void doSomething() {

    }

    public static class NewClass {
        public static void sing(){
            System.out.println("LALALA");
        }
    }
}
