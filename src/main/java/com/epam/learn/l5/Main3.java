package com.epam.learn.l5;

public class Main3 {
    public static void main(String[] args) {
        String ex="barsik";
        //read String pool
        String ex3="barsik"; //При создании проверяется, есть ли такое значение в стринг пуле, и если есть, используется этот обьект
        String ex2="bar"+"sik";
        String ex4=new String("barsik").intern(); // intern - возвращает каноническое представление строки, это эквивалентно String ex="barsik";

        System.out.println(ex.concat(ex2));

        System.out.println(ex=="barsik");
        System.out.println(ex==ex3);
        System.out.println(ex==ex2);
        System.out.println(ex==ex4);

    }
}
