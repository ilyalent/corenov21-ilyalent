package com.epam.learn.l5;

import java.util.Arrays;

public class Main5 {
    public static void main(String[] args) {
        String name = "Barsik";
        String name2= "Bar";
        name2=name2+"sik";

        System.out.println(name==name2); // == только для сравнения примитивов и enum
        System.out.println(name);
        System.out.println(name2);

        System.out.println(name.equals(name2));

        System.out.println(name.toLowerCase());
        System.out.println(name.toUpperCase());

        System.out.println(name.indexOf("r")); //индекс первого вхождения в строку

        String value="Abrakadabra";
        System.out.println(value.indexOf("a", 6));
        System.out.println(value.indexOf(97));
        System.out.println(value.lastIndexOf(97)); // последнее вхождение
        System.out.println(value.lastIndexOf("abr"));

        System.out.println(value.replace("a", " ")); // меняет все совпадения, при этом создает новую строку
        System.out.println(value.replaceAll("a", " ")); //replaceAll - меняет все совпадения по regEx
        System.out.println(value.replaceFirst("a", "")); //меняет первое совпадение


        System.out.println(value.contains("akk"));
        System.out.println(value.startsWith("Ab"));
        System.out.println(value.endsWith("Ab"));
        System.out.println(value.endsWith("ra"));

        System.out.println("  a ss ".trim());
        System.out.println("barsik".substring(2));
        System.out.println(String.valueOf(2L));

        System.out.println(Arrays.toString("barsik;murzik;pushok".split(";")));

    }
}
