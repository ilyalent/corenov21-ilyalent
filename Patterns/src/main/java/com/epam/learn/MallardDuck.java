package com.epam.learn;

public class MallardDuck extends Duck{
    public MallardDuck() {
        flyable = new FlyWithWings();
    }

    @Override
    void display() {
        System.out.println("im mallard duck");
    }
}
