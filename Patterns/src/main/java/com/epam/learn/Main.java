package com.epam.learn;

public class Main {
    public static void main(String[] args) {
        Duck mallardDuck = new MallardDuck();

        mallardDuck.performFly();
    }
}
